#ifndef ROCKET_H
#define ROCKET_H

#include "ExternalHeaders.h"
#include "Model.h"
#include "Earth.h"

class Rocket {
public:
    Rocket(Earth *earth, bool *keyStates);

    void update(float timeScale);

    void render();

    vec3 *positionVector;
    float altitude;
private:
    const float DRAG_COEFICIENT = 0.5;
    const float STARTING_WEIGHT = 45000/*kg*/;
    const vec3 STARTING_POSITION = vec3(0, 0, 23622048);
    const vec3 STARTING_DIRECTION = vec3(0, 0, 1);
    const float MAX_ENGINE_THRUST = 10000000/*N*/;
    const float MIN_REFERENCE_AREA = 3/*m^2*/;
    const float MAX_REFERENCE_AREA = 10/*m^2*/;
    const float THROTTLE_STEP = 0.5;
    const float ROTATION_SPEED = 60;

    float getDragForce();

    float getReferenceArea();

    void printFlightData();

    void updateKeyEvents(float timeScale);

    void updateMechanics(float timeScale);

    void drawVectors();

    bool *keyStates;


    vec3 velocityVector;
    vec3 direction_rollAxis;
    vec3 yawAxis;
    vec3 pitchAxis;
    float roll;
    float weight;
    float thrust;
    float gForce;
    float gravAcceleration;
    Model model;
    Earth *earth;
    float throttle;
};

#endif // !ROCKET_H