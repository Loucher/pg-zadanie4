#include "Rocket.h"

Rocket::Rocket(Earth *earth, bool *keyStates) {
    Rocket::earth = earth;
    Rocket::keyStates = keyStates;
    model.load("rocket.obj", "rocket.mtl");
    positionVector = new vec3(STARTING_POSITION);
    positionVector->z = positionVector->z + 187;
    roll = 0;
    throttle = 0;
    direction_rollAxis = vec3(0, 0, 1);
    yawAxis = vec3(0, 1, 0);
    pitchAxis = vec3(1, 0, 0);
    velocityVector = vec3(0, 0, 0);
    weight = STARTING_WEIGHT;
}

void Rocket::update(float timeScale) {
    updateKeyEvents(timeScale);
    updateMechanics(timeScale);
}

void Rocket::render() {
    glPushMatrix();
    glTranslatef(positionVector->x, positionVector->y, positionVector->z);
    glRotatef(roll, direction_rollAxis.x, direction_rollAxis.y, direction_rollAxis.z);
    vec3 axis = glm::cross(STARTING_DIRECTION, direction_rollAxis);
    float angle = glm::angle(STARTING_DIRECTION, direction_rollAxis);
    angle *= 180 / M_PI;//radians to angles
    glRotatef(angle, axis.x, axis.y, axis.z);
    glRotatef(90, 1, 0, 0);//rotate mesh to proper orientation
    model.render();
    glPopMatrix();
    drawVectors();
    printFlightData();
}

float Rocket::getDragForce() {
    float altitude = earth->getAltitude(positionVector);
    float airDensity = earth->getAirDensity(altitude);
    float velocity = glm::distance(vec3(0, 0, 0), velocityVector);
    return (airDensity * pow(velocity, 2) * getReferenceArea() * DRAG_COEFICIENT) / 2;
}

float Rocket::getReferenceArea() {
    float angle = glm::angle(velocityVector, direction_rollAxis);
    return MIN_REFERENCE_AREA + sin(angle) * (MAX_REFERENCE_AREA - MIN_REFERENCE_AREA);
}

void Rocket::printFlightData() {
    float velocity = glm::length(velocityVector) / 60;
    system("cls");
    if (velocity != 0) {
        cout << "Altitude:" << altitude << "m" << endl;
        cout << "Velocity:" << velocity << "m/s2" << endl;
        cout << "Throttle:" << throttle * 100 << "%" << endl;
        cout << "Thrust:" << thrust << "N" << endl;
        cout << "G:" << gravAcceleration << "m/s2" << endl;
        cout << "Gforce:" << gForce << "N" << endl;
        cout << "Drag:" << getDragForce() << "N" << endl;
        cout << "Air Density:" << earth->getAirDensity(altitude) << endl;
    }
    else {
        cout << "Space Simulator" << endl << endl;
        cout << "Roll: Q  E - red vector" << endl;
        cout << "Pitch: W  S - blue vector" << endl;
        cout << "Yaw: A  D - green vector" << endl;
        cout << "Throttle: I  K" << endl;
        cout << "Left mouse click + drag: camera rotation" << endl;
        cout << "Right mouse click + drag up/down: camera zoom in/out" << endl << endl;
        cout << "Increase throttle to START or T for teleporting to Low Earth Orbit" << endl;
    }
}


void Rocket::drawVectors() {
    glDisable(GL_TEXTURE_2D);

    glColor4f(1, 0, 0, 1);
    static const float red[] = {1, 0, 0, 1};
    static const float green[] = {0, 1, 0, 1};
    static const float blue[] = {0, 0, 1, 1};
    static const float orange[] = {1, 0.5f, 0, 1};
    static const float black[] = {0, 0, 0, 1};
    float axisLenght = 500;
    glLineWidth(2);

    // direction_rollAxis
    glMaterialfv(GL_FRONT, GL_EMISSION, red);
    glBegin(GL_LINES);
    glVertex3f(positionVector->x, positionVector->y, positionVector->z);
    glVertex3f(positionVector->x + direction_rollAxis.x * axisLenght,
            positionVector->y + direction_rollAxis.y * axisLenght,
            positionVector->z + direction_rollAxis.z * axisLenght);
    glEnd();

    // direction_rollAxis
    glMaterialfv(GL_FRONT, GL_EMISSION, green);
    glBegin(GL_LINES);
    glVertex3f(positionVector->x, positionVector->y, positionVector->z);
    glVertex3f(positionVector->x + yawAxis.x * axisLenght,
            positionVector->y + yawAxis.y * axisLenght,
            positionVector->z + yawAxis.z * axisLenght);
    glEnd();

    // pitchAxis
    glMaterialfv(GL_FRONT, GL_EMISSION, blue);
    glBegin(GL_LINES);
    glVertex3f(positionVector->x, positionVector->y, positionVector->z);
    glVertex3f(positionVector->x + pitchAxis.x * axisLenght,
            positionVector->y + pitchAxis.y * axisLenght,
            positionVector->z + pitchAxis.z * axisLenght);
    glEnd();

    // velocity
    vec3 velNormal = normalize(velocityVector);
    glMaterialfv(GL_FRONT, GL_EMISSION, orange);
    glBegin(GL_LINES);
    glVertex3f(positionVector->x, positionVector->y, positionVector->z);
    glVertex3f(positionVector->x + velNormal.x * axisLenght * 1.1,
            positionVector->y + velNormal.y * axisLenght * 1.1,
            positionVector->z + velNormal.z * axisLenght * 1.1);
    glEnd();

    glMaterialfv(GL_FRONT, GL_EMISSION, black);
    glEnable(GL_TEXTURE_2D);
}

void Rocket::updateKeyEvents(float timeScale) {
    float rotation = (float) (timeScale * ROTATION_SPEED * M_PI / 180);
    if (keyStates['q'] || keyStates['Q']) {
        //roll-
        roll -= timeScale * ROTATION_SPEED;
        pitchAxis = glm::rotate(pitchAxis, -rotation, direction_rollAxis);
        yawAxis = glm::rotate(yawAxis, -rotation, direction_rollAxis);
    }
    if (keyStates['e'] || keyStates['E']) {
        //roll+
        roll += timeScale * ROTATION_SPEED;
        pitchAxis = glm::rotate(pitchAxis, rotation, direction_rollAxis);
        yawAxis = glm::rotate(yawAxis, rotation, direction_rollAxis);
    }

    if (keyStates['w'] || keyStates['W']) {
        //pitch +
        direction_rollAxis = glm::rotate(direction_rollAxis, rotation, pitchAxis);
        yawAxis = glm::rotate(yawAxis, rotation, pitchAxis);
    }
    if (keyStates['s'] || keyStates['S']) {
        //pitch -
        direction_rollAxis = glm::rotate(direction_rollAxis, -rotation, pitchAxis);
        yawAxis = glm::rotate(yawAxis, -rotation, pitchAxis);
    }

    if (keyStates['d'] || keyStates['D']) {
        //yaw +
        pitchAxis = glm::rotate(pitchAxis, -rotation, yawAxis);
        direction_rollAxis = glm::rotate(direction_rollAxis, -rotation, yawAxis);
    }

    if (keyStates['a'] || keyStates['A']) {
        //yaw +
        pitchAxis = glm::rotate(pitchAxis, rotation, yawAxis);
        direction_rollAxis = glm::rotate(direction_rollAxis, rotation, yawAxis);
    }

    if (keyStates['i'] || keyStates['I']) {
        throttle += THROTTLE_STEP * timeScale;
        if (throttle > 1) throttle = 1;
    }
    if (keyStates['k'] || keyStates['K']) {
        throttle -= THROTTLE_STEP * timeScale;
        if (throttle < 0) throttle = 0;
    }
    if (keyStates['t'] || keyStates['T']) {
        vec3 newPosition = STARTING_POSITION * 1.1f;
        positionVector->x = newPosition.x;
        positionVector->y = newPosition.y;
        positionVector->z = newPosition.z;
    }
}

void Rocket::updateMechanics(float timeScale) {
    altitude = earth->getAltitude(positionVector) / 60;
    thrust = MAX_ENGINE_THRUST * throttle;
    vec3 thrustVector = glm::normalize(direction_rollAxis) * thrust;

    gravAcceleration = earth->getGh(altitude);
    if (earth->getAltitude(positionVector) > 188) {
        gForce = earth->getGh(altitude) * weight;
    }
    else {
        gForce = 0;
    }
    vec3 gVector = glm::normalize(*positionVector) * gForce;

    vec3 dragVector;
    if (glm::length(velocityVector) != 0) {
        dragVector = glm::normalize(velocityVector) * getDragForce();
    }
    else {
        dragVector = vec3(0, 0, 0);
    }

    velocityVector += ((thrustVector - gVector - dragVector) / weight) * timeScale;
    *positionVector += (velocityVector * timeScale);
    if (earth->getAltitude(positionVector) < 187) {
        velocityVector = vec3(0, 0, 0);
        vec3 positionNormal = glm::normalize(*positionVector);
        *positionVector = (earth->RADIUS + 187) * positionNormal;
    }
}
