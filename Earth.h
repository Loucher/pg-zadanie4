#ifndef EARTH_H
#define EARTH_H

#include "ExternalHeaders.h"
#include "Model.h"

class Earth {
public:
    Earth();

    void render(float altitude);

    const string TEXTURE_NAME = "earth.bmp";
    const vec3 POSITION = vec3(0, 0, 0);
    const float RADIUS = 23622048/* OpenGL units */;
    const float EARTH_RADIUS = 6371000/*m*/;
    const float EQUATORIAL_G = 9.80665f/* ms^-2 */;
    const int SUBDIVISIONS = 400;

    const float SEA_LEVEL_ATM_PRESS = 101325/* Pa */;
    const float SEA_LEVEL_STAND_TEMP = 288.15f/* K */;
    const float SEA_LEVEL_AIR_DENSITY = 1.21f;
    const float TEMP_LAPSE_RATE = 0.0065f/* K/m */;
    const float GAS_CONSTANT = 8.31447f/* J/(mol*K) */;
    const float MOLAR_MASS = 0.0289644f/* kg/mol */;

    float getGh(float altitude);

    float getAirDensity(float altitude);

    float getAltitude(vec3 *position);

private:
    GLUquadricObj *sphere = NULL;
    GLuint _textureId;
};

#endif // !EARTH_H