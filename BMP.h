#ifndef BMP_H
#define BMP_H

#include "ExternalHeaders.h"

#pragma pack(push, 1)

class Header {
public:
    /* Magic number for file */
    unsigned short bfType;
    /* Size of file */
    unsigned int bfSize;
    /* Reserved */
    unsigned short bfReserved1;
    /* ... */
    unsigned short bfReserved2;
    /* Offset to bitmap data */
    unsigned int bfOffBits;
    /* Size of info header */
    unsigned int biSize;
    /* Width of image */
    int biWidth;
    /* Height of image */
    int biHeight;
    /* Number of color planes */
    unsigned short biPlanes;
    /* Number of bits per pixel */
    unsigned short biBitCount;
    /* Type of compression to use */
    unsigned int biCompression;
    /* Size of image data */
    unsigned int biSizeImage;
    /* X pixels per meter */
    int biXPelsPerMeter;
    /* Y pixels per meter */
    int biYPelsPerMeter;
    /* Number of colors used */
    unsigned int biClrUsed;
    /* Number of important colors */
    unsigned int biClrImportant;
};

#pragma pack(pop)

class BMP {
public:
    int width, height, channels;
    unsigned char *raw_file;

    BMP(string filename);

    ~BMP();
};

#endif // !BMP_H
