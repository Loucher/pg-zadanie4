#ifndef MODEL_H
#define MODEL_H

#include "ExternalHeaders.h"
#include "TextureManager.h"

using namespace boost::algorithm;

struct OBJObject;
struct OBJFace;
struct OBJVertex;
struct OBJNormal;
struct OBJTexture;
struct OBJFaceItem;
struct OBJMaterial;

struct OBJFace {
    vector<OBJFaceItem> items;
    string material;
};

struct OBJFaceItem {
    OBJFaceItem() {
        vertexIndex = 0;
        normalIndex = 0;
        textureIndex = 0;
    }

    unsigned int vertexIndex;
    unsigned int normalIndex;
    unsigned int textureIndex;
};

struct OBJVertex {
    OBJVertex() {
        coords[0] = 0;
        coords[1] = 0;
        coords[2] = 0;
    }

    float coords[3];
};

struct OBJNormal {
    OBJNormal() {
        coords[0] = 0;
        coords[1] = 0;
        coords[2] = 0;
    }

    float coords[3];
};

struct OBJTexture {
    OBJTexture() {
        coords[0] = 0;
        coords[1] = 0;
        coords[2] = 0;
    }

    float coords[3];
};

struct OBJMaterial {
    float Ka[4];
    float Kd[4];
    float Ks[4];

    GLuint texture;

    float texScaleU;
    float texScaleV;
};

struct OBJObject {
    OBJObject() {
        vertices.push_back(OBJVertex());
        normals.push_back(OBJNormal());
        textures.push_back(OBJTexture());
        computedNormals.push_back(OBJNormal());
    }

    vector<OBJVertex> vertices;
    vector<OBJNormal> computedNormals;
    vector<OBJNormal> normals;
    vector<OBJTexture> textures;
    vector<OBJFace> faces;
    map<string, OBJMaterial> materials;
};

class Model {
public:
    Model();

    ~Model();

    bool load(const string &mesh_filename, const string &material_filename = "");

    void render(bool withBoundingBox = false) const;

    const vec3 &minimumVertex() const;

    const vec3 &maximumVertex() const;

protected:
    void processMeshLine(const string &line);

    void processMaterialLine(const string &line);

    void build();

    void renderBoundingBox() const;

private:
    OBJObject object_;
    GLuint listId_;
    string currentMaterial_;
    TextureManager textureManager_;
    vec3 minimumVertex_;
    vec3 maximumVertex_;

    void chop(string &str);

    void split(const string &str, char del, vector<string> &tokens);
};

#endif // !MODEL_H