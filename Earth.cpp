#include "Earth.h"

Earth::Earth() {
    sphere = gluNewQuadric();
    gluQuadricDrawStyle(sphere, GLU_FILL);
    gluQuadricTexture(sphere, GL_TRUE);
    gluQuadricNormals(sphere, GLU_SMOOTH);
    TextureManager textureManager;
    string temp = ASSETS_DIR + TEXTURE_NAME;
    textureManager.load_texture(ASSETS_DIR + TEXTURE_NAME);
    _textureId = textureManager.get_texture(ASSETS_DIR + TEXTURE_NAME);
}


void Earth::render(float altitude) {
    glPushMatrix();
    glTranslatef(POSITION.x, POSITION.y, POSITION.z);
    glRotatef(-90, 1, 0, 0);
    glRotatef(30, 0, 1, 0);
    glBindTexture(GL_TEXTURE_2D, _textureId);
    gluSphere(sphere, RADIUS, SUBDIVISIONS, SUBDIVISIONS);
    glPopMatrix();
    float colorDensity = getAirDensity(altitude) / SEA_LEVEL_AIR_DENSITY;
    glClearColor(0.529f * colorDensity, 0.807f * colorDensity, 0.921f * colorDensity, 1);
}

/**
* return altitude, OpenGL units
*/
float Earth::getAltitude(vec3 *position) {
    return glm::distance(POSITION, *position) - RADIUS;
}

/**
* calculate gravitational acceleration at given altitude, ms^-2
*/
float Earth::getGh(float altitude) {
    return EQUATORIAL_G * pow(EARTH_RADIUS / (EARTH_RADIUS + altitude), 2);
}

/**
* calculate air density at given altitude, kg/m^3
*/
float Earth::getAirDensity(float altitude) {

//this model worked only up to altitude 86km :(
//    float temperature = SEA_LEVEL_STAND_TEMP - TEMP_LAPSE_RATE * altitude;
//    float pressure = SEA_LEVEL_ATM_PRESS * pow(1 - (TEMP_LAPSE_RATE * altitude) / SEA_LEVEL_ATM_PRESS, (EQUATORIAL_G * MOLAR_MASS) / GAS_CONSTANT * TEMP_LAPSE_RATE);
//    return (pressure * MOLAR_MASS) / (GAS_CONSTANT * temperature);
    //approximate air density
    return pow(SEA_LEVEL_AIR_DENSITY, -altitude / 4000);
}
