#ifndef CAMERA_H
#define CAMERA_H

#include "ExternalHeaders.h"

class Camera {
public:
    Camera(vec3 *lookAt);

    void mouseListener(int button, int state, int x, int y);

    void mouseDragListener(int x, int y);

    void render();

private:
    vec3 *lookAt;
    vec3 orientationVector;
    vec3 pitchVector;
    vec3 yawVector;
    float distance;
    float oldX;
    float oldY;
    int cameraMode;
    const int NO_MODE = 0;
    const int ROTATE_MODE = 1;
    const int ZOOM_MODE = 2;
    const float ROTATE_SCALE = 0.1f;
    const float ZOOM_SCALE = 1;
    const float DEFAULT_DISTANCE = 500;
    const float MIN_DISTANCE = 400;
    const float MAX_DISTANCE = 3000;
    GLUquadricObj *sphere = NULL;
    GLuint navBallTexture;
    GLuint vectorsTexture;
};

#endif // !CAMERA_H
