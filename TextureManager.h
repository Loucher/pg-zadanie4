#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include "ExternalHeaders.h"
#include "BMP.h"

class TextureManager {
public:
    TextureManager();

    ~TextureManager();

public:
    bool load_texture(const std::string &filename, bool clamp = false);

    bool release_texture(const std::string &filename);

    GLuint get_texture(const std::string &filename);

private:
    typedef std::map<std::string, GLuint> TextureList;
    TextureList textures;
};

#endif // !RESOURCE_MANAGER_H
