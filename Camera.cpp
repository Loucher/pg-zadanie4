#include "Camera.h"

Camera::Camera(vec3 *lookAt) {
    Camera::lookAt = lookAt;
    orientationVector = vec3(0, 0, 1);
    yawVector = vec3(0, 1, 0);
    pitchVector = vec3(1, 0, 0);
    oldX = -1;
    oldY = -1;
    distance = DEFAULT_DISTANCE;
}

void Camera::mouseListener(int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        cameraMode = NO_MODE;
    }
    if (button == 0 && state == GLUT_DOWN) {
        cameraMode = ROTATE_MODE;
    }
    if (button == 2 && state == GLUT_DOWN) {
        cameraMode = ZOOM_MODE;
    }
    oldX = x;
    oldY = y;
}

void Camera::mouseDragListener(int x, int y) {
    float yaw = 0;
    float pitch = 0;
    if (cameraMode == ROTATE_MODE) {
        yaw = (float) (((oldX - x) * ROTATE_SCALE) * M_PI / 180);
        pitch = (float) (((oldY - y) * ROTATE_SCALE) * M_PI / 180);
    }
    if (cameraMode == ZOOM_MODE) {
        distance += ((oldY - y) * ZOOM_SCALE);
        if (distance > MAX_DISTANCE) {
            distance = MAX_DISTANCE;
        } else if (distance<MIN_DISTANCE) {
            distance = MIN_DISTANCE;
        }
    }
    orientationVector = glm::rotate(orientationVector, pitch, pitchVector);
    yawVector = glm::rotate(yawVector, pitch, pitchVector);

    orientationVector = glm::rotate(orientationVector, yaw, yawVector);
    pitchVector = glm::rotate(pitchVector, yaw, yawVector);

    oldX = x;
    oldY = y;
}

void Camera::render() {
    vec3 position = orientationVector * distance;
    gluLookAt(
            lookAt->x + position.x,
            lookAt->y + position.y,
            lookAt->z + position.z,
            lookAt->x,
            lookAt->y,
            lookAt->z,
            yawVector.x,
            yawVector.y,
            yawVector.z
    );
}