#ifndef EXTERNAL_HEADERS_H
#define EXTERNAL_HEADERS_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <limits>
#include <locale>
#include <functional>
#include <algorithm>
#include <sstream>
#include <math.h>

#include <boost/algorithm/string.hpp>

#define GLM_FORCE_RADIANS
#include <glm/vec3.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/quaternion.hpp>

#include <GL/glut.h>

#ifndef M_PI
#   define M_PI 3.14159265358979323846
#endif

#ifndef GL_CLAMP_TO_EDGE
#   define GL_CLAMP_TO_EDGE 0x812F
#endif


#define ASSETS_DIR "assets/"

using namespace std;
using namespace glm;

#endif