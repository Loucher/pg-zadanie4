#include "BMP.h"

BMP::BMP(string filename) {
    //load image from file
    FILE *file_bmp;
    raw_file = NULL;
    file_bmp = fopen(filename.c_str(), "rb");

    Header header;

    //load up header
    fread(&header, 1, sizeof(header), file_bmp);

    //set height and width
    width = header.biWidth;
    height = header.biHeight;
    channels = header.biBitCount / 8;
    //move to image data ( in case of different header size than our implementation)
    fseek(file_bmp, (long) header.bfOffBits, SEEK_SET);

    //allocate memory
    //raw_file = new unsigned char[header.biSizeImage];
    raw_file = (unsigned char *) malloc(header.biSizeImage);

    //read raw image data
    fread(raw_file, 1, header.biSizeImage, file_bmp);
    fclose(file_bmp);
    unsigned char B;
    for (unsigned int i = 0; i < header.biSizeImage; i += channels) {
        B = raw_file[i];
        raw_file[i] = raw_file[i + 2];
        raw_file[i + 2] = B;
    }
};

BMP::~BMP() {
    delete[] raw_file;
}