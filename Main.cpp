#include "ExternalHeaders.h"
#include "Camera.h"
#include "Earth.h"
#include "Rocket.h"

using namespace std;

static const unsigned int windowWidth = 1024;
static const unsigned int windowHeight = 768;
static const float SUN_POSITION[] = {0, 0, 1, 0};
bool *keyStates = new bool[256];
Camera *camera;
Earth *earth;
Rocket *rocket;
int prevTime = 0;

void init(void) {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, SUN_POSITION);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glDisable(GL_COLOR_MATERIAL);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glDepthMask(GL_TRUE);
    glEnable(GL_CULL_FACE);

    glClearColor(0, 0, 0, 0);

    for (int i = 0; i < 256; i++) {
        keyStates[i] = false;
    }
}


void onDisplay(void) {
    float difftime = glutGet(GLUT_ELAPSED_TIME) - prevTime;
    float timescale = difftime / 1000;
    prevTime = glutGet(GLUT_ELAPSED_TIME);
    rocket->update(timescale);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_NORMALIZE);
    glViewport(0, 0, windowWidth, windowHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float) windowWidth / (float) windowHeight, 100, 2362048 * 3);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    camera->render();
    glLightfv(GL_LIGHT0, GL_POSITION, SUN_POSITION);

    earth->render(rocket->altitude);
    rocket->render();

    glutSwapBuffers();
}

void motionFunction(int x, int y) {
    camera->mouseDragListener(x, y);
}

void mouseFunction(int button, int state, int x, int y) {
    camera->mouseListener(button, state, x, y);
}

void keyPressed(unsigned char key, int x, int y) {
    keyStates[key] = true;
}

void keyUp(unsigned char key, int x, int y) {
    keyStates[key] = false;
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(windowWidth, windowHeight);
    glutCreateWindow("Zadanie4");
    init();
    earth = new Earth();
    rocket = new Rocket(earth, keyStates);
    camera = new Camera(rocket->positionVector);
    glutMotionFunc(motionFunction);
    glutMouseFunc(mouseFunction);
    glutKeyboardFunc(keyPressed);
    glutKeyboardUpFunc(keyUp);
    glutDisplayFunc(onDisplay);
    glutIdleFunc(onDisplay);
    glutMainLoop();
    return 0;
}