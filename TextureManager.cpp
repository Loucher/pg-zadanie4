#include "TextureManager.h"

TextureManager::TextureManager() {
}

TextureManager::~TextureManager() {
}

bool TextureManager::load_texture(const std::string &filename, bool clamp) {
    // ---State preservation---
    GLuint previous_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint *) &previous_texture);

    // Generate a new texture id
    GLuint texture_id;
    glGenTextures(1, &texture_id);

    glBindTexture(GL_TEXTURE_2D, texture_id);

    BMP *bmp = new BMP(filename);
    if (bmp->channels == 3) {
        //RGB
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp->width, bmp->height, 0, GL_RGB, GL_UNSIGNED_BYTE, bmp->raw_file);
    } else if (bmp->channels == 4) {
        //RGBA
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bmp->width, bmp->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp->raw_file);
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    if (clamp) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    else // repeat
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    // Add the texture to the list of loaded one
    textures.insert(std::make_pair(filename, texture_id));

    // ---State preservation---
    glBindTexture(GL_TEXTURE_2D, previous_texture);
    return true;
}

bool TextureManager::release_texture(const std::string &filename) {
    // Retrieve texture id
    GLuint texture_id = get_texture(filename);

    if (texture_id != 0) {
        glDeleteTextures(1, &texture_id); // Texture found
        textures.erase(filename);
        return true;
    }
    else {
        std::cerr << "Warning: cannot release non loaded texture \"" << filename << "\"" << std::endl;
        return false;
    }
}

GLuint TextureManager::get_texture(const std::string &filename) {
    TextureList::const_iterator it = textures.find(filename);

    if (it == textures.end()) {
        std::cerr << "Warning: cannot retrieve non loaded texture \"" << filename << "\"" << std::endl;
        return 0;   // Texture not found
    }
    else
        return it->second; // Texture found
}
